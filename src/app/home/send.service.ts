import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})

export class SendService {
  url='https://emotions-app.herokuapp.com/';
  constructor(private http: HttpClient) { }
  sendData(data) {
    return this.http.post(this.url+'ode',data);
  }
  getData(){
    return this.http.get(this.url+'ode');
  }
}
