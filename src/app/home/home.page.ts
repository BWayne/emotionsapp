import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { SendService } from './send.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(public toastController: ToastController, public send: SendService) { }

  showSlider = false;
  showHistory = false;
  showMenu = true;
  odes: any;

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    pager: false,
    scrollbar: true

  };
  slideItems = [
    {
      "name": "Affection",
      "description": "Affection reproaches, but does not denounce.",
      "icon": "ec-heart-eyes"
    },
    {
      "name": "Angry",
      "description": "For every minute you remain angry, you give up sixty seconds of peace of mind.",
      "icon": "ec-imp"
    },
    {
      "name": "Crying",
      "description": "I was smiling yesterday,I am smiling today and I will smile tomorrow.Simply because life is too short to cry for anything.",
      "icon": "ec-cry"
    },
    {
      "name": "Depression",
      "description": "Having anxiety and depression is like being scared and tired at the same time. It's the fear of failure, but no urge to be productive.",
      "icon": "ec-pensive"
    },
    {
      "name": "Doubt",
      "description": "Doubt … is an illness that comes from knowledge and leads to madness.",
      "icon": "ec-unamused"
    },
    {
      "name": "Emotion",
      "description": "Unexpressed emotions will never die. They are buried alive and will come forth later in uglier ways.",
      "icon": "ec-frowning-woman"
    },
    {
      "name": "Jealous",
      "description": "Jealousy, that dragon which slays love under the pretence of keeping it alive.",
      "icon": "ec-sweat"
    }
  ];
  message = "working";
  tempArray = {};
  today = Date.now()

  ionSlideTap(e) {
    console.log("e", e.srcElement.swiper)
    let lastIndex = e.srcElement.swiper.isEnd;
    if (lastIndex) {
      console.log("last Index");
      console.log("Values", this.tempArray);
      this.presentToast();
    }
    else {
      e.srcElement.swiper.slideNext()
    }
  }
  buttonTap(name, value) {
    console.log("e", name, value);
    this.tempArray[name.toLowerCase()] = value;
  }
  fabHisClicked() {
    console.log("History Fab Click Received!");
    this.showHistory = false;
    this.showMenu = true;
  }
  fabClicked() {
    console.log("Fab Click Received!");
    this.showSlider = !this.showSlider;
    this.showMenu = !this.showMenu;
    console.log("Slider Value", this.showSlider);
  }
  async responseToast() {
    const toast = await this.toastController.create({
      message: 'Your emotions have been saved.',
      buttons: [
        {
          side: 'end',
          icon: 'close',
          text: 'CLOSE',
          handler: () => {
            this.showSlider = false;
            this.showMenu = true;
          }
        }]
    });
    toast.present();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      header: 'Do You wanna',
      message: 'Save the Data?',
      position: 'bottom',
      buttons: [
        {
          side: 'start',
          icon: 'star',
          text: 'YES',
          handler: () => {
            console.log('Yes clicked');
            this.send.sendData(this.tempArray).subscribe((response) => {
              console.log("response", response);
              this.responseToast();
            });
          }
        }, {
          text: 'NO',
          role: 'cancel',
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    toast.present();
  }
  fullHistory() {
    this.showMenu = !this.showMenu
    this.showHistory = true;
    this.send.getData().subscribe((response) => {
      console.log("received Data", response);
      this.odes = response;
    });
  }

}
